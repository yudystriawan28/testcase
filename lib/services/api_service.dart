import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class ApiClient {
  final Dio _dio;

  ApiClient(this._dio) {
    _dio.options.baseUrl = 'https://imdb8.p.rapidapi.com';
    _dio.options.connectTimeout = const Duration(seconds: 60).inMilliseconds;
  }

  Future<Response> get(
    String path, {
    Map<String, dynamic>? headers,
    Map<String, dynamic>? params,
    bool followRedirects = true,
    bool Function(int?)? validateStatus,
    String? contentType,
  }) async {
    return _dio.get(
      path,
      options: Options(
        headers: headers,
        validateStatus: validateStatus,
        followRedirects: followRedirects,
        contentType: contentType,
      ),
      queryParameters: params,
    );
  }
}
