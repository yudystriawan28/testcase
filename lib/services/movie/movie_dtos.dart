import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:majootestcase/models/domain/movie/movie.dart';
import 'package:majootestcase/models/domain/movie/value_objects.dart';

part 'movie_dtos.freezed.dart';
part 'movie_dtos.g.dart';

@freezed
class MovieDto with _$MovieDto {
  const MovieDto._();
  const factory MovieDto({
    @JsonKey(name: 'id') required String id,
    @JsonKey(name: 'i') required MovieImageDto image,
    @JsonKey(name: 'l') required String title,
    @JsonKey(name: 'q') required String category,
    @JsonKey(name: 'v') List<MovieSeriesDto>? series,
    @JsonKey(name: 'y') required int year,
  }) = _MovieDto;

  factory MovieDto.fromJson(Map<String, dynamic> json) =>
      _$MovieDtoFromJson(json);

  Movie toDomain() {
    return Movie(
      title: MovieTitle(title),
      poster: MoviePoster(image.imageUrl),
      category: MovieCategory(category),
      year: MovieYear(year.toString()),
      series: series?.map((e) => e.toDomain()).toImmutableList() ??
          const KtList.empty(),
    );
  }
}

@freezed
class MovieImageDto with _$MovieImageDto {
  const MovieImageDto._();
  const factory MovieImageDto({
    @JsonKey(name: 'height') required int height,
    @JsonKey(name: 'width') required int width,
    @JsonKey(name: 'imageUrl') required String imageUrl,
  }) = _MovieImageDto;

  factory MovieImageDto.fromJson(Map<String, dynamic> json) =>
      _$MovieImageDtoFromJson(json);
}

@freezed
class MovieSeriesDto with _$MovieSeriesDto {
  const MovieSeriesDto._();
  const factory MovieSeriesDto({
    @JsonKey(name: 'id') required String id,
    @JsonKey(name: 'i') required MovieImageDto image,
    @JsonKey(name: 'l') required String title,
  }) = _MovieSeriesDto;

  factory MovieSeriesDto.fromJson(Map<String, dynamic> json) =>
      _$MovieSeriesDtoFromJson(json);

  MovieSeries toDomain() {
    return MovieSeries(
      poster: MoviePoster(image.imageUrl),
      title: MovieTitle(title),
    );
  }
}
