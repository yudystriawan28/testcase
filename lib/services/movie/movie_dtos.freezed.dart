// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'movie_dtos.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MovieDto _$MovieDtoFromJson(Map<String, dynamic> json) {
  return _MovieDto.fromJson(json);
}

/// @nodoc
class _$MovieDtoTearOff {
  const _$MovieDtoTearOff();

  _MovieDto call(
      {@JsonKey(name: 'id') required String id,
      @JsonKey(name: 'i') required MovieImageDto image,
      @JsonKey(name: 'l') required String title,
      @JsonKey(name: 'q') required String category,
      @JsonKey(name: 'v') List<MovieSeriesDto>? series,
      @JsonKey(name: 'y') required int year}) {
    return _MovieDto(
      id: id,
      image: image,
      title: title,
      category: category,
      series: series,
      year: year,
    );
  }

  MovieDto fromJson(Map<String, Object?> json) {
    return MovieDto.fromJson(json);
  }
}

/// @nodoc
const $MovieDto = _$MovieDtoTearOff();

/// @nodoc
mixin _$MovieDto {
  @JsonKey(name: 'id')
  String get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'i')
  MovieImageDto get image => throw _privateConstructorUsedError;
  @JsonKey(name: 'l')
  String get title => throw _privateConstructorUsedError;
  @JsonKey(name: 'q')
  String get category => throw _privateConstructorUsedError;
  @JsonKey(name: 'v')
  List<MovieSeriesDto>? get series => throw _privateConstructorUsedError;
  @JsonKey(name: 'y')
  int get year => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MovieDtoCopyWith<MovieDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieDtoCopyWith<$Res> {
  factory $MovieDtoCopyWith(MovieDto value, $Res Function(MovieDto) then) =
      _$MovieDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') String id,
      @JsonKey(name: 'i') MovieImageDto image,
      @JsonKey(name: 'l') String title,
      @JsonKey(name: 'q') String category,
      @JsonKey(name: 'v') List<MovieSeriesDto>? series,
      @JsonKey(name: 'y') int year});

  $MovieImageDtoCopyWith<$Res> get image;
}

/// @nodoc
class _$MovieDtoCopyWithImpl<$Res> implements $MovieDtoCopyWith<$Res> {
  _$MovieDtoCopyWithImpl(this._value, this._then);

  final MovieDto _value;
  // ignore: unused_field
  final $Res Function(MovieDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? image = freezed,
    Object? title = freezed,
    Object? category = freezed,
    Object? series = freezed,
    Object? year = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as MovieImageDto,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      category: category == freezed
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as String,
      series: series == freezed
          ? _value.series
          : series // ignore: cast_nullable_to_non_nullable
              as List<MovieSeriesDto>?,
      year: year == freezed
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }

  @override
  $MovieImageDtoCopyWith<$Res> get image {
    return $MovieImageDtoCopyWith<$Res>(_value.image, (value) {
      return _then(_value.copyWith(image: value));
    });
  }
}

/// @nodoc
abstract class _$MovieDtoCopyWith<$Res> implements $MovieDtoCopyWith<$Res> {
  factory _$MovieDtoCopyWith(_MovieDto value, $Res Function(_MovieDto) then) =
      __$MovieDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') String id,
      @JsonKey(name: 'i') MovieImageDto image,
      @JsonKey(name: 'l') String title,
      @JsonKey(name: 'q') String category,
      @JsonKey(name: 'v') List<MovieSeriesDto>? series,
      @JsonKey(name: 'y') int year});

  @override
  $MovieImageDtoCopyWith<$Res> get image;
}

/// @nodoc
class __$MovieDtoCopyWithImpl<$Res> extends _$MovieDtoCopyWithImpl<$Res>
    implements _$MovieDtoCopyWith<$Res> {
  __$MovieDtoCopyWithImpl(_MovieDto _value, $Res Function(_MovieDto) _then)
      : super(_value, (v) => _then(v as _MovieDto));

  @override
  _MovieDto get _value => super._value as _MovieDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? image = freezed,
    Object? title = freezed,
    Object? category = freezed,
    Object? series = freezed,
    Object? year = freezed,
  }) {
    return _then(_MovieDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as MovieImageDto,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      category: category == freezed
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as String,
      series: series == freezed
          ? _value.series
          : series // ignore: cast_nullable_to_non_nullable
              as List<MovieSeriesDto>?,
      year: year == freezed
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MovieDto extends _MovieDto {
  const _$_MovieDto(
      {@JsonKey(name: 'id') required this.id,
      @JsonKey(name: 'i') required this.image,
      @JsonKey(name: 'l') required this.title,
      @JsonKey(name: 'q') required this.category,
      @JsonKey(name: 'v') this.series,
      @JsonKey(name: 'y') required this.year})
      : super._();

  factory _$_MovieDto.fromJson(Map<String, dynamic> json) =>
      _$$_MovieDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final String id;
  @override
  @JsonKey(name: 'i')
  final MovieImageDto image;
  @override
  @JsonKey(name: 'l')
  final String title;
  @override
  @JsonKey(name: 'q')
  final String category;
  @override
  @JsonKey(name: 'v')
  final List<MovieSeriesDto>? series;
  @override
  @JsonKey(name: 'y')
  final int year;

  @override
  String toString() {
    return 'MovieDto(id: $id, image: $image, title: $title, category: $category, series: $series, year: $year)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _MovieDto &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.image, image) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.category, category) &&
            const DeepCollectionEquality().equals(other.series, series) &&
            const DeepCollectionEquality().equals(other.year, year));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(image),
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(category),
      const DeepCollectionEquality().hash(series),
      const DeepCollectionEquality().hash(year));

  @JsonKey(ignore: true)
  @override
  _$MovieDtoCopyWith<_MovieDto> get copyWith =>
      __$MovieDtoCopyWithImpl<_MovieDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MovieDtoToJson(this);
  }
}

abstract class _MovieDto extends MovieDto {
  const factory _MovieDto(
      {@JsonKey(name: 'id') required String id,
      @JsonKey(name: 'i') required MovieImageDto image,
      @JsonKey(name: 'l') required String title,
      @JsonKey(name: 'q') required String category,
      @JsonKey(name: 'v') List<MovieSeriesDto>? series,
      @JsonKey(name: 'y') required int year}) = _$_MovieDto;
  const _MovieDto._() : super._();

  factory _MovieDto.fromJson(Map<String, dynamic> json) = _$_MovieDto.fromJson;

  @override
  @JsonKey(name: 'id')
  String get id;
  @override
  @JsonKey(name: 'i')
  MovieImageDto get image;
  @override
  @JsonKey(name: 'l')
  String get title;
  @override
  @JsonKey(name: 'q')
  String get category;
  @override
  @JsonKey(name: 'v')
  List<MovieSeriesDto>? get series;
  @override
  @JsonKey(name: 'y')
  int get year;
  @override
  @JsonKey(ignore: true)
  _$MovieDtoCopyWith<_MovieDto> get copyWith =>
      throw _privateConstructorUsedError;
}

MovieImageDto _$MovieImageDtoFromJson(Map<String, dynamic> json) {
  return _MovieImageDto.fromJson(json);
}

/// @nodoc
class _$MovieImageDtoTearOff {
  const _$MovieImageDtoTearOff();

  _MovieImageDto call(
      {@JsonKey(name: 'height') required int height,
      @JsonKey(name: 'width') required int width,
      @JsonKey(name: 'imageUrl') required String imageUrl}) {
    return _MovieImageDto(
      height: height,
      width: width,
      imageUrl: imageUrl,
    );
  }

  MovieImageDto fromJson(Map<String, Object?> json) {
    return MovieImageDto.fromJson(json);
  }
}

/// @nodoc
const $MovieImageDto = _$MovieImageDtoTearOff();

/// @nodoc
mixin _$MovieImageDto {
  @JsonKey(name: 'height')
  int get height => throw _privateConstructorUsedError;
  @JsonKey(name: 'width')
  int get width => throw _privateConstructorUsedError;
  @JsonKey(name: 'imageUrl')
  String get imageUrl => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MovieImageDtoCopyWith<MovieImageDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieImageDtoCopyWith<$Res> {
  factory $MovieImageDtoCopyWith(
          MovieImageDto value, $Res Function(MovieImageDto) then) =
      _$MovieImageDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'height') int height,
      @JsonKey(name: 'width') int width,
      @JsonKey(name: 'imageUrl') String imageUrl});
}

/// @nodoc
class _$MovieImageDtoCopyWithImpl<$Res>
    implements $MovieImageDtoCopyWith<$Res> {
  _$MovieImageDtoCopyWithImpl(this._value, this._then);

  final MovieImageDto _value;
  // ignore: unused_field
  final $Res Function(MovieImageDto) _then;

  @override
  $Res call({
    Object? height = freezed,
    Object? width = freezed,
    Object? imageUrl = freezed,
  }) {
    return _then(_value.copyWith(
      height: height == freezed
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as int,
      width: width == freezed
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as int,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$MovieImageDtoCopyWith<$Res>
    implements $MovieImageDtoCopyWith<$Res> {
  factory _$MovieImageDtoCopyWith(
          _MovieImageDto value, $Res Function(_MovieImageDto) then) =
      __$MovieImageDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'height') int height,
      @JsonKey(name: 'width') int width,
      @JsonKey(name: 'imageUrl') String imageUrl});
}

/// @nodoc
class __$MovieImageDtoCopyWithImpl<$Res>
    extends _$MovieImageDtoCopyWithImpl<$Res>
    implements _$MovieImageDtoCopyWith<$Res> {
  __$MovieImageDtoCopyWithImpl(
      _MovieImageDto _value, $Res Function(_MovieImageDto) _then)
      : super(_value, (v) => _then(v as _MovieImageDto));

  @override
  _MovieImageDto get _value => super._value as _MovieImageDto;

  @override
  $Res call({
    Object? height = freezed,
    Object? width = freezed,
    Object? imageUrl = freezed,
  }) {
    return _then(_MovieImageDto(
      height: height == freezed
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as int,
      width: width == freezed
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as int,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MovieImageDto extends _MovieImageDto {
  const _$_MovieImageDto(
      {@JsonKey(name: 'height') required this.height,
      @JsonKey(name: 'width') required this.width,
      @JsonKey(name: 'imageUrl') required this.imageUrl})
      : super._();

  factory _$_MovieImageDto.fromJson(Map<String, dynamic> json) =>
      _$$_MovieImageDtoFromJson(json);

  @override
  @JsonKey(name: 'height')
  final int height;
  @override
  @JsonKey(name: 'width')
  final int width;
  @override
  @JsonKey(name: 'imageUrl')
  final String imageUrl;

  @override
  String toString() {
    return 'MovieImageDto(height: $height, width: $width, imageUrl: $imageUrl)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _MovieImageDto &&
            const DeepCollectionEquality().equals(other.height, height) &&
            const DeepCollectionEquality().equals(other.width, width) &&
            const DeepCollectionEquality().equals(other.imageUrl, imageUrl));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(height),
      const DeepCollectionEquality().hash(width),
      const DeepCollectionEquality().hash(imageUrl));

  @JsonKey(ignore: true)
  @override
  _$MovieImageDtoCopyWith<_MovieImageDto> get copyWith =>
      __$MovieImageDtoCopyWithImpl<_MovieImageDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MovieImageDtoToJson(this);
  }
}

abstract class _MovieImageDto extends MovieImageDto {
  const factory _MovieImageDto(
      {@JsonKey(name: 'height') required int height,
      @JsonKey(name: 'width') required int width,
      @JsonKey(name: 'imageUrl') required String imageUrl}) = _$_MovieImageDto;
  const _MovieImageDto._() : super._();

  factory _MovieImageDto.fromJson(Map<String, dynamic> json) =
      _$_MovieImageDto.fromJson;

  @override
  @JsonKey(name: 'height')
  int get height;
  @override
  @JsonKey(name: 'width')
  int get width;
  @override
  @JsonKey(name: 'imageUrl')
  String get imageUrl;
  @override
  @JsonKey(ignore: true)
  _$MovieImageDtoCopyWith<_MovieImageDto> get copyWith =>
      throw _privateConstructorUsedError;
}

MovieSeriesDto _$MovieSeriesDtoFromJson(Map<String, dynamic> json) {
  return _MovieSeriesDto.fromJson(json);
}

/// @nodoc
class _$MovieSeriesDtoTearOff {
  const _$MovieSeriesDtoTearOff();

  _MovieSeriesDto call(
      {@JsonKey(name: 'id') required String id,
      @JsonKey(name: 'i') required MovieImageDto image,
      @JsonKey(name: 'l') required String title}) {
    return _MovieSeriesDto(
      id: id,
      image: image,
      title: title,
    );
  }

  MovieSeriesDto fromJson(Map<String, Object?> json) {
    return MovieSeriesDto.fromJson(json);
  }
}

/// @nodoc
const $MovieSeriesDto = _$MovieSeriesDtoTearOff();

/// @nodoc
mixin _$MovieSeriesDto {
  @JsonKey(name: 'id')
  String get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'i')
  MovieImageDto get image => throw _privateConstructorUsedError;
  @JsonKey(name: 'l')
  String get title => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MovieSeriesDtoCopyWith<MovieSeriesDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieSeriesDtoCopyWith<$Res> {
  factory $MovieSeriesDtoCopyWith(
          MovieSeriesDto value, $Res Function(MovieSeriesDto) then) =
      _$MovieSeriesDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') String id,
      @JsonKey(name: 'i') MovieImageDto image,
      @JsonKey(name: 'l') String title});

  $MovieImageDtoCopyWith<$Res> get image;
}

/// @nodoc
class _$MovieSeriesDtoCopyWithImpl<$Res>
    implements $MovieSeriesDtoCopyWith<$Res> {
  _$MovieSeriesDtoCopyWithImpl(this._value, this._then);

  final MovieSeriesDto _value;
  // ignore: unused_field
  final $Res Function(MovieSeriesDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? image = freezed,
    Object? title = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as MovieImageDto,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }

  @override
  $MovieImageDtoCopyWith<$Res> get image {
    return $MovieImageDtoCopyWith<$Res>(_value.image, (value) {
      return _then(_value.copyWith(image: value));
    });
  }
}

/// @nodoc
abstract class _$MovieSeriesDtoCopyWith<$Res>
    implements $MovieSeriesDtoCopyWith<$Res> {
  factory _$MovieSeriesDtoCopyWith(
          _MovieSeriesDto value, $Res Function(_MovieSeriesDto) then) =
      __$MovieSeriesDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') String id,
      @JsonKey(name: 'i') MovieImageDto image,
      @JsonKey(name: 'l') String title});

  @override
  $MovieImageDtoCopyWith<$Res> get image;
}

/// @nodoc
class __$MovieSeriesDtoCopyWithImpl<$Res>
    extends _$MovieSeriesDtoCopyWithImpl<$Res>
    implements _$MovieSeriesDtoCopyWith<$Res> {
  __$MovieSeriesDtoCopyWithImpl(
      _MovieSeriesDto _value, $Res Function(_MovieSeriesDto) _then)
      : super(_value, (v) => _then(v as _MovieSeriesDto));

  @override
  _MovieSeriesDto get _value => super._value as _MovieSeriesDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? image = freezed,
    Object? title = freezed,
  }) {
    return _then(_MovieSeriesDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as MovieImageDto,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MovieSeriesDto extends _MovieSeriesDto {
  const _$_MovieSeriesDto(
      {@JsonKey(name: 'id') required this.id,
      @JsonKey(name: 'i') required this.image,
      @JsonKey(name: 'l') required this.title})
      : super._();

  factory _$_MovieSeriesDto.fromJson(Map<String, dynamic> json) =>
      _$$_MovieSeriesDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final String id;
  @override
  @JsonKey(name: 'i')
  final MovieImageDto image;
  @override
  @JsonKey(name: 'l')
  final String title;

  @override
  String toString() {
    return 'MovieSeriesDto(id: $id, image: $image, title: $title)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _MovieSeriesDto &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.image, image) &&
            const DeepCollectionEquality().equals(other.title, title));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(image),
      const DeepCollectionEquality().hash(title));

  @JsonKey(ignore: true)
  @override
  _$MovieSeriesDtoCopyWith<_MovieSeriesDto> get copyWith =>
      __$MovieSeriesDtoCopyWithImpl<_MovieSeriesDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MovieSeriesDtoToJson(this);
  }
}

abstract class _MovieSeriesDto extends MovieSeriesDto {
  const factory _MovieSeriesDto(
      {@JsonKey(name: 'id') required String id,
      @JsonKey(name: 'i') required MovieImageDto image,
      @JsonKey(name: 'l') required String title}) = _$_MovieSeriesDto;
  const _MovieSeriesDto._() : super._();

  factory _MovieSeriesDto.fromJson(Map<String, dynamic> json) =
      _$_MovieSeriesDto.fromJson;

  @override
  @JsonKey(name: 'id')
  String get id;
  @override
  @JsonKey(name: 'i')
  MovieImageDto get image;
  @override
  @JsonKey(name: 'l')
  String get title;
  @override
  @JsonKey(ignore: true)
  _$MovieSeriesDtoCopyWith<_MovieSeriesDto> get copyWith =>
      throw _privateConstructorUsedError;
}
