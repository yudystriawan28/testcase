// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_dtos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MovieDto _$$_MovieDtoFromJson(Map<String, dynamic> json) => _$_MovieDto(
      id: json['id'] as String,
      image: MovieImageDto.fromJson(json['i'] as Map<String, dynamic>),
      title: json['l'] as String,
      category: json['q'] as String,
      series: (json['v'] as List<dynamic>?)
          ?.map((e) => MovieSeriesDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      year: json['y'] as int,
    );

Map<String, dynamic> _$$_MovieDtoToJson(_$_MovieDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'i': instance.image.toJson(),
      'l': instance.title,
      'q': instance.category,
      'v': instance.series?.map((e) => e.toJson()).toList(),
      'y': instance.year,
    };

_$_MovieImageDto _$$_MovieImageDtoFromJson(Map<String, dynamic> json) =>
    _$_MovieImageDto(
      height: json['height'] as int,
      width: json['width'] as int,
      imageUrl: json['imageUrl'] as String,
    );

Map<String, dynamic> _$$_MovieImageDtoToJson(_$_MovieImageDto instance) =>
    <String, dynamic>{
      'height': instance.height,
      'width': instance.width,
      'imageUrl': instance.imageUrl,
    };

_$_MovieSeriesDto _$$_MovieSeriesDtoFromJson(Map<String, dynamic> json) =>
    _$_MovieSeriesDto(
      id: json['id'] as String,
      image: MovieImageDto.fromJson(json['i'] as Map<String, dynamic>),
      title: json['l'] as String,
    );

Map<String, dynamic> _$$_MovieSeriesDtoToJson(_$_MovieSeriesDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'i': instance.image.toJson(),
      'l': instance.title,
    };
