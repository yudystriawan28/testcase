import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/collection.dart';
import 'package:majootestcase/models/domain/exception/exceptions.dart';
import 'package:majootestcase/models/domain/movie/i_movie_repository.dart';
import 'package:majootestcase/models/domain/movie/movie.dart';
import 'package:majootestcase/services/movie/data_sources/remote_data_provider.dart';

@Injectable(as: IMovieRepository)
class MovieRepository implements IMovieRepository {
  final MovieRemoteDataProvider _remoteDataProvider;

  MovieRepository(this._remoteDataProvider);

  @override
  Future<Either<AppException, KtList<Movie>>> loadMovies() async {
    try {
      final result = await _remoteDataProvider.getMovies();

      return result.fold(
        (f) => left(f),
        (movieDtos) {
          final movies = movieDtos.map((e) => e.toDomain()).toImmutableList();
          return right(movies);
        },
      );
    } catch (e, s) {
      log(
        'MovieRepository',
        name: 'loadMovies',
        error: e,
        stackTrace: s,
      );
      return left(AppException.unexpected(errorMessage: e.toString()));
    }
  }
}
