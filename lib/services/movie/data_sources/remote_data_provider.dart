import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import 'package:majootestcase/models/domain/exception/exceptions.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/services/movie/movie_dtos.dart';
import 'package:majootestcase/services/network/network_client.dart';
import 'package:majootestcase/utils/constant.dart';

@injectable
class MovieRemoteDataProvider {
  final ApiClient _apiClient;
  final NetworkClient _networkClient;

  MovieRemoteDataProvider(
    this._apiClient,
    this._networkClient,
  );

  Future<Either<AppException, List<MovieDto>>> getMovies() async {
    try {
      final hasConnection = await _networkClient.isConnected;

      if (!hasConnection) {
        return left(const AppException.badNetwork());
      }

      final response = await _apiClient.get(
        '/auto-complete',
        params: {'q': 'game of thr'},
        headers: {
          'x-rapidapi-host': kRapidapiHost,
          'x-rapidapi-key': kRapidapiKey,
        },
      );

      if (response.statusCode == 200) {
        final dtos = (response.data['d'] as List)
            .map((e) => MovieDto.fromJson(e))
            .toList();
        return right(dtos);
      }

      return left(
          AppException.serverError(errorMessage: response.statusMessage));
    } catch (e, s) {
      log(
        'MovieRemoteDataProvider',
        name: 'getMovies',
        error: e,
        stackTrace: s,
      );
      return left(AppException.unexpected(errorMessage: e.toString()));
    }
  }
}
