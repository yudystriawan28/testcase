import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:majootestcase/models/domain/core/value_objects.dart';
import 'package:majootestcase/models/domain/user/user.dart';
import 'package:majootestcase/models/domain/user/value_objects.dart';
import 'package:majootestcase/services/database/database.dart' as local;

part 'user_dtos.freezed.dart';
part 'user_dtos.g.dart';

@freezed
class UserDto with _$UserDto {
  const UserDto._();
  const factory UserDto({
    @JsonKey(name: 'id') required String id,
    @JsonKey(name: 'username') required String username,
    @JsonKey(name: 'email') required String email,
    @JsonKey(name: 'password') required String password,
  }) = _UserDto;

  factory UserDto.fromJson(Map<String, dynamic> json) =>
      _$UserDtoFromJson(json);

  factory UserDto.fromDomain(User user) => UserDto(
        id: user.id.getOrCrash(),
        username: user.username.getOrCrash(),
        email: user.email.getOrCrash(),
        password: user.password.getOrCrash(),
      );

  factory UserDto.fromLocal(local.User user) => UserDto(
        id: user.id,
        username: user.username,
        email: user.email,
        password: user.password,
      );

  User toDomain() {
    return User(
      id: UniqueId.fromUniqueString(id),
      username: Username(username),
      email: Email(email),
      password: Password(password),
    );
  }

  local.User toLocal() {
    return local.User(
      id: id,
      email: email,
      username: username,
      password: password,
    );
  }
}
