import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/domain/exception/exceptions.dart';
import 'package:majootestcase/services/database/database.dart';

@injectable
class AuthLocalDataProvider {
  final MyDatabase _db;

  AuthLocalDataProvider(this._db);

  Future<User?> getUser(String tokenStr) {
    return _db.userDao.findByToken(tokenStr);
  }

  Future<Either<AppException, String?>> login({
    required String email,
    required String password,
  }) async {
    try {
      //get token
      final entity = await _db.userDao.findByEmailAndPassword(
        email: email,
        password: password,
      );
      if (entity != null) {
        return right(entity.token);
      }

      return left(const AppException.unauthenticated());
    } catch (e, s) {
      log(
        'AuthLocalDataProvider',
        name: 'addUser',
        error: e,
        stackTrace: s,
      );
      return left(const AppException.unexpected());
    }
  }

  Future<Either<AppException, Unit>> register(User user) async {
    try {
      final entry = user.toCompanion(true);

      await _db.userDao.insertUser(entry);

      return right(unit);
    } catch (e, s) {
      log(
        'AuthLocalDataProvider',
        name: 'addUser',
        error: e,
        stackTrace: s,
      );
      return left(const AppException.unexpected());
    }
  }
}
