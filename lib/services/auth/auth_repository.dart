import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/domain/core/value_objects.dart';
import 'package:majootestcase/models/domain/user/user.dart';
import 'package:majootestcase/services/user/user_dtos.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:majootestcase/models/domain/auth/i_auth_repository.dart';
import 'package:majootestcase/models/domain/exception/exceptions.dart';
import 'package:majootestcase/models/domain/user/value_objects.dart';
import 'package:majootestcase/services/auth/data_sources/local_data_provider.dart';

@Injectable(as: IAuthRepository)
class AuthRepository implements IAuthRepository {
  final AuthLocalDataProvider _localDataProvider;
  final SharedPreferences _prefs;

  AuthRepository(
    this._localDataProvider,
    this._prefs,
  );

  @override
  Future<Either<AppException, Unit>> login({
    required Email email,
    required Password password,
  }) async {
    try {
      final response = await _localDataProvider.login(
        email: email.getOrCrash(),
        password: password.getOrCrash(),
      );

      return response.fold((f) => left(f), (token) {
        if (token != null) {
          _prefs.setString(kToken, token);
          return right(unit);
        }

        return left(
            const AppException.unexpected(errorMessage: 'token not found'));
      });
    } catch (e, s) {
      log(
        'AuthRepository',
        name: 'login',
        error: e,
        stackTrace: s,
      );
      return left(const AppException.unexpected());
    }
  }

  @override
  Future<Either<AppException, Unit>> logout() async {
    await _prefs.clear();
    return right(unit);
  }

  @override
  Future<Either<AppException, Unit>> register(User user) async {
    try {
      final entity = UserDto.fromDomain(user).toLocal();

      final response = await _localDataProvider.register(entity);

      return response.fold(
        (f) => left(f),
        (_) => right(unit),
      );
    } catch (e, s) {
      log(
        'AuthRepository',
        name: 'register',
        error: e,
        stackTrace: s,
      );
      return left(const AppException.unexpected());
    }
  }

  @override
  Future<Either<AppException, User>> loadUser(UniqueId token) async {
    try {
      final user = await _localDataProvider.getUser(token.getOrCrash());

      if (user != null) {
        final userDomain = UserDto.fromLocal(user).toDomain();
        return right(userDomain);
      }

      return left(const AppException.unauthenticated());
    } catch (e, s) {
      log(
        'AuthRepository',
        name: 'loadUser',
        error: e,
        stackTrace: s,
      );
      return left(const AppException.unexpected());
    }
  }
}
