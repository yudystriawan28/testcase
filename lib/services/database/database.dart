import 'package:drift/drift.dart';
import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

part 'daos/user_dao.dart';
part 'database.g.dart';
part 'tables.dart';

@DriftDatabase(
  tables: [
    Users,
    Movies,
  ],
  daos: [
    UserDao,
  ],
)
class MyDatabase extends _$MyDatabase {
  MyDatabase.connect(DatabaseConnection c) : super.connect(c);

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration => MigrationStrategy(
        beforeOpen: (details) async {
          // reset local table
          if (kDebugMode) {
            final m = createMigrator();
            for (var table in allTables) {
              await m.deleteTable(table.actualTableName);
              await m.createTable(table);
            }
          }
        },
      );
}
