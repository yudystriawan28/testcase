part of '../database.dart';

@DriftAccessor(tables: [
  Users,
])
class UserDao extends DatabaseAccessor<MyDatabase> with _$UserDaoMixin {
  UserDao(MyDatabase db) : super(db);

  Future<User?> findByToken(String tokenStr) {
    return (select(users)..where((tbl) => tbl.token.equals(tokenStr)))
        .getSingleOrNull();
  }

  Future<User?> findByEmail(String emailStr) {
    return (select(users)..where((tbl) => tbl.email.equals(emailStr)))
        .getSingleOrNull();
  }

  Future<User?> findByEmailAndPassword({
    required String email,
    required String password,
  }) {
    return (select(users)
          ..where(
              (tbl) => tbl.email.equals(email) & tbl.password.equals(password)))
        .getSingleOrNull();
  }

  Future<void> insertUser(UsersCompanion entry) {
    return into(users).insert(entry);
  }
}
