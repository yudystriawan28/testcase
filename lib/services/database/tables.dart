part of 'database.dart';

class Users extends Table {
  TextColumn get id => text()();
  TextColumn get email => text().customConstraint('UNIQUE')();
  TextColumn get username => text().customConstraint('UNIQUE')();
  TextColumn get password => text()();
  TextColumn get token =>
      text().nullable().clientDefault(() => const Uuid().v4())();

  @override
  Set<Column>? get primaryKey => {id};
}

class Movies extends Table {
  TextColumn get id => text()();
  TextColumn get title => text()();
  TextColumn get poster => text().nullable()();

  @override
  Set<Column>? get primaryKey => {id};
}
