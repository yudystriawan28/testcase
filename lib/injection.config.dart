// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:connectivity_plus/connectivity_plus.dart' as _i3;
import 'package:dio/dio.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i7;

import 'bloc/auth/auth_cubit.dart' as _i13;
import 'bloc/auth/form/auth_form_cubit.dart' as _i14;
import 'bloc/movie/cubit/movie_loader_cubit.dart' as _i17;
import 'di/connectivity_di.dart' as _i18;
import 'di/database_di.dart' as _i20;
import 'di/dio_di.dart' as _i19;
import 'di/shared_preferences_di.dart' as _i21;
import 'models/domain/auth/i_auth_repository.dart' as _i10;
import 'models/domain/movie/i_movie_repository.dart' as _i15;
import 'services/api_service.dart' as _i8;
import 'services/auth/auth_repository.dart' as _i11;
import 'services/auth/data_sources/local_data_provider.dart' as _i9;
import 'services/database/database.dart' as _i5;
import 'services/movie/data_sources/remote_data_provider.dart' as _i12;
import 'services/movie/movie_repository.dart' as _i16;
import 'services/network/network_client.dart'
    as _i6; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final connectivityDi = _$ConnectivityDi();
  final dioDi = _$DioDi();
  final databaseDi = _$DatabaseDi();
  final sharedPreferencesDi = _$SharedPreferencesDi();
  gh.lazySingleton<_i3.Connectivity>(() => connectivityDi.connectivity);
  gh.lazySingleton<_i4.Dio>(() => dioDi.dio);
  gh.singleton<_i5.MyDatabase>(databaseDi.db);
  gh.lazySingleton<_i6.NetworkClient>(
      () => _i6.NetworkClient(get<_i3.Connectivity>()));
  await gh.factoryAsync<_i7.SharedPreferences>(() => sharedPreferencesDi.prefs,
      preResolve: true);
  gh.lazySingleton<_i8.ApiClient>(() => _i8.ApiClient(get<_i4.Dio>()));
  gh.factory<_i9.AuthLocalDataProvider>(
      () => _i9.AuthLocalDataProvider(get<_i5.MyDatabase>()));
  gh.factory<_i10.IAuthRepository>(() => _i11.AuthRepository(
      get<_i9.AuthLocalDataProvider>(), get<_i7.SharedPreferences>()));
  gh.factory<_i12.MovieRemoteDataProvider>(() => _i12.MovieRemoteDataProvider(
      get<_i8.ApiClient>(), get<_i6.NetworkClient>()));
  gh.factory<_i13.AuthCubit>(() => _i13.AuthCubit(
      get<_i7.SharedPreferences>(), get<_i10.IAuthRepository>()));
  gh.factory<_i14.AuthFormCubit>(
      () => _i14.AuthFormCubit(get<_i10.IAuthRepository>()));
  gh.factory<_i15.IMovieRepository>(
      () => _i16.MovieRepository(get<_i12.MovieRemoteDataProvider>()));
  gh.factory<_i17.MovieLoaderCubit>(
      () => _i17.MovieLoaderCubit(get<_i15.IMovieRepository>()));
  return get;
}

class _$ConnectivityDi extends _i18.ConnectivityDi {}

class _$DioDi extends _i19.DioDi {}

class _$DatabaseDi extends _i20.DatabaseDi {}

class _$SharedPreferencesDi extends _i21.SharedPreferencesDi {}
