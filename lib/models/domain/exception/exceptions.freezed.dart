// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'exceptions.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AppExceptionTearOff {
  const _$AppExceptionTearOff();

  _ServerError serverError({String? errorMessage}) {
    return _ServerError(
      errorMessage: errorMessage,
    );
  }

  _Unecpected unexpected({String? errorMessage}) {
    return _Unecpected(
      errorMessage: errorMessage,
    );
  }

  _Unauthenticated unauthenticated() {
    return const _Unauthenticated();
  }

  _BadNetwork badNetwork() {
    return const _BadNetwork();
  }
}

/// @nodoc
const $AppException = _$AppExceptionTearOff();

/// @nodoc
mixin _$AppException {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? errorMessage) serverError,
    required TResult Function(String? errorMessage) unexpected,
    required TResult Function() unauthenticated,
    required TResult Function() badNetwork,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_Unecpected value) unexpected,
    required TResult Function(_Unauthenticated value) unauthenticated,
    required TResult Function(_BadNetwork value) badNetwork,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppExceptionCopyWith<$Res> {
  factory $AppExceptionCopyWith(
          AppException value, $Res Function(AppException) then) =
      _$AppExceptionCopyWithImpl<$Res>;
}

/// @nodoc
class _$AppExceptionCopyWithImpl<$Res> implements $AppExceptionCopyWith<$Res> {
  _$AppExceptionCopyWithImpl(this._value, this._then);

  final AppException _value;
  // ignore: unused_field
  final $Res Function(AppException) _then;
}

/// @nodoc
abstract class _$ServerErrorCopyWith<$Res> {
  factory _$ServerErrorCopyWith(
          _ServerError value, $Res Function(_ServerError) then) =
      __$ServerErrorCopyWithImpl<$Res>;
  $Res call({String? errorMessage});
}

/// @nodoc
class __$ServerErrorCopyWithImpl<$Res> extends _$AppExceptionCopyWithImpl<$Res>
    implements _$ServerErrorCopyWith<$Res> {
  __$ServerErrorCopyWithImpl(
      _ServerError _value, $Res Function(_ServerError) _then)
      : super(_value, (v) => _then(v as _ServerError));

  @override
  _ServerError get _value => super._value as _ServerError;

  @override
  $Res call({
    Object? errorMessage = freezed,
  }) {
    return _then(_ServerError(
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_ServerError implements _ServerError {
  const _$_ServerError({this.errorMessage});

  @override
  final String? errorMessage;

  @override
  String toString() {
    return 'AppException.serverError(errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ServerError &&
            const DeepCollectionEquality()
                .equals(other.errorMessage, errorMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(errorMessage));

  @JsonKey(ignore: true)
  @override
  _$ServerErrorCopyWith<_ServerError> get copyWith =>
      __$ServerErrorCopyWithImpl<_ServerError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? errorMessage) serverError,
    required TResult Function(String? errorMessage) unexpected,
    required TResult Function() unauthenticated,
    required TResult Function() badNetwork,
  }) {
    return serverError(errorMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
  }) {
    return serverError?.call(errorMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(errorMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_Unecpected value) unexpected,
    required TResult Function(_Unauthenticated value) unauthenticated,
    required TResult Function(_BadNetwork value) badNetwork,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class _ServerError implements AppException {
  const factory _ServerError({String? errorMessage}) = _$_ServerError;

  String? get errorMessage;
  @JsonKey(ignore: true)
  _$ServerErrorCopyWith<_ServerError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$UnecpectedCopyWith<$Res> {
  factory _$UnecpectedCopyWith(
          _Unecpected value, $Res Function(_Unecpected) then) =
      __$UnecpectedCopyWithImpl<$Res>;
  $Res call({String? errorMessage});
}

/// @nodoc
class __$UnecpectedCopyWithImpl<$Res> extends _$AppExceptionCopyWithImpl<$Res>
    implements _$UnecpectedCopyWith<$Res> {
  __$UnecpectedCopyWithImpl(
      _Unecpected _value, $Res Function(_Unecpected) _then)
      : super(_value, (v) => _then(v as _Unecpected));

  @override
  _Unecpected get _value => super._value as _Unecpected;

  @override
  $Res call({
    Object? errorMessage = freezed,
  }) {
    return _then(_Unecpected(
      errorMessage: errorMessage == freezed
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_Unecpected implements _Unecpected {
  const _$_Unecpected({this.errorMessage});

  @override
  final String? errorMessage;

  @override
  String toString() {
    return 'AppException.unexpected(errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Unecpected &&
            const DeepCollectionEquality()
                .equals(other.errorMessage, errorMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(errorMessage));

  @JsonKey(ignore: true)
  @override
  _$UnecpectedCopyWith<_Unecpected> get copyWith =>
      __$UnecpectedCopyWithImpl<_Unecpected>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? errorMessage) serverError,
    required TResult Function(String? errorMessage) unexpected,
    required TResult Function() unauthenticated,
    required TResult Function() badNetwork,
  }) {
    return unexpected(errorMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
  }) {
    return unexpected?.call(errorMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(errorMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_Unecpected value) unexpected,
    required TResult Function(_Unauthenticated value) unauthenticated,
    required TResult Function(_BadNetwork value) badNetwork,
  }) {
    return unexpected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
  }) {
    return unexpected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
    required TResult orElse(),
  }) {
    if (unexpected != null) {
      return unexpected(this);
    }
    return orElse();
  }
}

abstract class _Unecpected implements AppException {
  const factory _Unecpected({String? errorMessage}) = _$_Unecpected;

  String? get errorMessage;
  @JsonKey(ignore: true)
  _$UnecpectedCopyWith<_Unecpected> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$UnauthenticatedCopyWith<$Res> {
  factory _$UnauthenticatedCopyWith(
          _Unauthenticated value, $Res Function(_Unauthenticated) then) =
      __$UnauthenticatedCopyWithImpl<$Res>;
}

/// @nodoc
class __$UnauthenticatedCopyWithImpl<$Res>
    extends _$AppExceptionCopyWithImpl<$Res>
    implements _$UnauthenticatedCopyWith<$Res> {
  __$UnauthenticatedCopyWithImpl(
      _Unauthenticated _value, $Res Function(_Unauthenticated) _then)
      : super(_value, (v) => _then(v as _Unauthenticated));

  @override
  _Unauthenticated get _value => super._value as _Unauthenticated;
}

/// @nodoc

class _$_Unauthenticated implements _Unauthenticated {
  const _$_Unauthenticated();

  @override
  String toString() {
    return 'AppException.unauthenticated()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _Unauthenticated);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? errorMessage) serverError,
    required TResult Function(String? errorMessage) unexpected,
    required TResult Function() unauthenticated,
    required TResult Function() badNetwork,
  }) {
    return unauthenticated();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
  }) {
    return unauthenticated?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
    required TResult orElse(),
  }) {
    if (unauthenticated != null) {
      return unauthenticated();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_Unecpected value) unexpected,
    required TResult Function(_Unauthenticated value) unauthenticated,
    required TResult Function(_BadNetwork value) badNetwork,
  }) {
    return unauthenticated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
  }) {
    return unauthenticated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
    required TResult orElse(),
  }) {
    if (unauthenticated != null) {
      return unauthenticated(this);
    }
    return orElse();
  }
}

abstract class _Unauthenticated implements AppException {
  const factory _Unauthenticated() = _$_Unauthenticated;
}

/// @nodoc
abstract class _$BadNetworkCopyWith<$Res> {
  factory _$BadNetworkCopyWith(
          _BadNetwork value, $Res Function(_BadNetwork) then) =
      __$BadNetworkCopyWithImpl<$Res>;
}

/// @nodoc
class __$BadNetworkCopyWithImpl<$Res> extends _$AppExceptionCopyWithImpl<$Res>
    implements _$BadNetworkCopyWith<$Res> {
  __$BadNetworkCopyWithImpl(
      _BadNetwork _value, $Res Function(_BadNetwork) _then)
      : super(_value, (v) => _then(v as _BadNetwork));

  @override
  _BadNetwork get _value => super._value as _BadNetwork;
}

/// @nodoc

class _$_BadNetwork implements _BadNetwork {
  const _$_BadNetwork();

  @override
  String toString() {
    return 'AppException.badNetwork()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _BadNetwork);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? errorMessage) serverError,
    required TResult Function(String? errorMessage) unexpected,
    required TResult Function() unauthenticated,
    required TResult Function() badNetwork,
  }) {
    return badNetwork();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
  }) {
    return badNetwork?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? errorMessage)? serverError,
    TResult Function(String? errorMessage)? unexpected,
    TResult Function()? unauthenticated,
    TResult Function()? badNetwork,
    required TResult orElse(),
  }) {
    if (badNetwork != null) {
      return badNetwork();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_Unecpected value) unexpected,
    required TResult Function(_Unauthenticated value) unauthenticated,
    required TResult Function(_BadNetwork value) badNetwork,
  }) {
    return badNetwork(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
  }) {
    return badNetwork?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ServerError value)? serverError,
    TResult Function(_Unecpected value)? unexpected,
    TResult Function(_Unauthenticated value)? unauthenticated,
    TResult Function(_BadNetwork value)? badNetwork,
    required TResult orElse(),
  }) {
    if (badNetwork != null) {
      return badNetwork(this);
    }
    return orElse();
  }
}

abstract class _BadNetwork implements AppException {
  const factory _BadNetwork() = _$_BadNetwork;
}
