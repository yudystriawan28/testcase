import 'package:freezed_annotation/freezed_annotation.dart';

part 'exceptions.freezed.dart';

@freezed
class AppException with _$AppException {
  const factory AppException.serverError({
    String? errorMessage,
  }) = _ServerError;
  const factory AppException.unexpected({
    String? errorMessage,
  }) = _Unecpected;
  const factory AppException.unauthenticated() = _Unauthenticated;
  const factory AppException.badNetwork() = _BadNetwork;
}
