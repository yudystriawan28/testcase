import 'package:dartz/dartz.dart';
import 'package:majootestcase/models/domain/core/value_objects.dart';
import 'package:majootestcase/models/domain/exception/exceptions.dart';
import 'package:majootestcase/models/domain/user/user.dart';
import 'package:majootestcase/models/domain/user/value_objects.dart';

abstract class IAuthRepository {
  Future<Either<AppException, Unit>> login({
    required Email email,
    required Password password,
  });
  Future<Either<AppException, User>> loadUser(UniqueId token);
  Future<Either<AppException, Unit>> register(User user);
  Future<Either<AppException, Unit>> logout();
}
