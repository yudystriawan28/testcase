// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'movie.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$MovieTearOff {
  const _$MovieTearOff();

  _Movie call(
      {required MovieTitle title,
      required MoviePoster poster,
      required MovieCategory category,
      required MovieYear year,
      required KtList<MovieSeries> series}) {
    return _Movie(
      title: title,
      poster: poster,
      category: category,
      year: year,
      series: series,
    );
  }
}

/// @nodoc
const $Movie = _$MovieTearOff();

/// @nodoc
mixin _$Movie {
  MovieTitle get title => throw _privateConstructorUsedError;
  MoviePoster get poster => throw _privateConstructorUsedError;
  MovieCategory get category => throw _privateConstructorUsedError;
  MovieYear get year => throw _privateConstructorUsedError;
  KtList<MovieSeries> get series => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MovieCopyWith<Movie> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieCopyWith<$Res> {
  factory $MovieCopyWith(Movie value, $Res Function(Movie) then) =
      _$MovieCopyWithImpl<$Res>;
  $Res call(
      {MovieTitle title,
      MoviePoster poster,
      MovieCategory category,
      MovieYear year,
      KtList<MovieSeries> series});
}

/// @nodoc
class _$MovieCopyWithImpl<$Res> implements $MovieCopyWith<$Res> {
  _$MovieCopyWithImpl(this._value, this._then);

  final Movie _value;
  // ignore: unused_field
  final $Res Function(Movie) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? poster = freezed,
    Object? category = freezed,
    Object? year = freezed,
    Object? series = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as MovieTitle,
      poster: poster == freezed
          ? _value.poster
          : poster // ignore: cast_nullable_to_non_nullable
              as MoviePoster,
      category: category == freezed
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as MovieCategory,
      year: year == freezed
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as MovieYear,
      series: series == freezed
          ? _value.series
          : series // ignore: cast_nullable_to_non_nullable
              as KtList<MovieSeries>,
    ));
  }
}

/// @nodoc
abstract class _$MovieCopyWith<$Res> implements $MovieCopyWith<$Res> {
  factory _$MovieCopyWith(_Movie value, $Res Function(_Movie) then) =
      __$MovieCopyWithImpl<$Res>;
  @override
  $Res call(
      {MovieTitle title,
      MoviePoster poster,
      MovieCategory category,
      MovieYear year,
      KtList<MovieSeries> series});
}

/// @nodoc
class __$MovieCopyWithImpl<$Res> extends _$MovieCopyWithImpl<$Res>
    implements _$MovieCopyWith<$Res> {
  __$MovieCopyWithImpl(_Movie _value, $Res Function(_Movie) _then)
      : super(_value, (v) => _then(v as _Movie));

  @override
  _Movie get _value => super._value as _Movie;

  @override
  $Res call({
    Object? title = freezed,
    Object? poster = freezed,
    Object? category = freezed,
    Object? year = freezed,
    Object? series = freezed,
  }) {
    return _then(_Movie(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as MovieTitle,
      poster: poster == freezed
          ? _value.poster
          : poster // ignore: cast_nullable_to_non_nullable
              as MoviePoster,
      category: category == freezed
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as MovieCategory,
      year: year == freezed
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as MovieYear,
      series: series == freezed
          ? _value.series
          : series // ignore: cast_nullable_to_non_nullable
              as KtList<MovieSeries>,
    ));
  }
}

/// @nodoc

class _$_Movie implements _Movie {
  const _$_Movie(
      {required this.title,
      required this.poster,
      required this.category,
      required this.year,
      required this.series});

  @override
  final MovieTitle title;
  @override
  final MoviePoster poster;
  @override
  final MovieCategory category;
  @override
  final MovieYear year;
  @override
  final KtList<MovieSeries> series;

  @override
  String toString() {
    return 'Movie(title: $title, poster: $poster, category: $category, year: $year, series: $series)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Movie &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.poster, poster) &&
            const DeepCollectionEquality().equals(other.category, category) &&
            const DeepCollectionEquality().equals(other.year, year) &&
            const DeepCollectionEquality().equals(other.series, series));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(poster),
      const DeepCollectionEquality().hash(category),
      const DeepCollectionEquality().hash(year),
      const DeepCollectionEquality().hash(series));

  @JsonKey(ignore: true)
  @override
  _$MovieCopyWith<_Movie> get copyWith =>
      __$MovieCopyWithImpl<_Movie>(this, _$identity);
}

abstract class _Movie implements Movie {
  const factory _Movie(
      {required MovieTitle title,
      required MoviePoster poster,
      required MovieCategory category,
      required MovieYear year,
      required KtList<MovieSeries> series}) = _$_Movie;

  @override
  MovieTitle get title;
  @override
  MoviePoster get poster;
  @override
  MovieCategory get category;
  @override
  MovieYear get year;
  @override
  KtList<MovieSeries> get series;
  @override
  @JsonKey(ignore: true)
  _$MovieCopyWith<_Movie> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
class _$MovieSeriesTearOff {
  const _$MovieSeriesTearOff();

  _MovieSeries call({required MoviePoster poster, required MovieTitle title}) {
    return _MovieSeries(
      poster: poster,
      title: title,
    );
  }
}

/// @nodoc
const $MovieSeries = _$MovieSeriesTearOff();

/// @nodoc
mixin _$MovieSeries {
  MoviePoster get poster => throw _privateConstructorUsedError;
  MovieTitle get title => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MovieSeriesCopyWith<MovieSeries> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieSeriesCopyWith<$Res> {
  factory $MovieSeriesCopyWith(
          MovieSeries value, $Res Function(MovieSeries) then) =
      _$MovieSeriesCopyWithImpl<$Res>;
  $Res call({MoviePoster poster, MovieTitle title});
}

/// @nodoc
class _$MovieSeriesCopyWithImpl<$Res> implements $MovieSeriesCopyWith<$Res> {
  _$MovieSeriesCopyWithImpl(this._value, this._then);

  final MovieSeries _value;
  // ignore: unused_field
  final $Res Function(MovieSeries) _then;

  @override
  $Res call({
    Object? poster = freezed,
    Object? title = freezed,
  }) {
    return _then(_value.copyWith(
      poster: poster == freezed
          ? _value.poster
          : poster // ignore: cast_nullable_to_non_nullable
              as MoviePoster,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as MovieTitle,
    ));
  }
}

/// @nodoc
abstract class _$MovieSeriesCopyWith<$Res>
    implements $MovieSeriesCopyWith<$Res> {
  factory _$MovieSeriesCopyWith(
          _MovieSeries value, $Res Function(_MovieSeries) then) =
      __$MovieSeriesCopyWithImpl<$Res>;
  @override
  $Res call({MoviePoster poster, MovieTitle title});
}

/// @nodoc
class __$MovieSeriesCopyWithImpl<$Res> extends _$MovieSeriesCopyWithImpl<$Res>
    implements _$MovieSeriesCopyWith<$Res> {
  __$MovieSeriesCopyWithImpl(
      _MovieSeries _value, $Res Function(_MovieSeries) _then)
      : super(_value, (v) => _then(v as _MovieSeries));

  @override
  _MovieSeries get _value => super._value as _MovieSeries;

  @override
  $Res call({
    Object? poster = freezed,
    Object? title = freezed,
  }) {
    return _then(_MovieSeries(
      poster: poster == freezed
          ? _value.poster
          : poster // ignore: cast_nullable_to_non_nullable
              as MoviePoster,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as MovieTitle,
    ));
  }
}

/// @nodoc

class _$_MovieSeries implements _MovieSeries {
  const _$_MovieSeries({required this.poster, required this.title});

  @override
  final MoviePoster poster;
  @override
  final MovieTitle title;

  @override
  String toString() {
    return 'MovieSeries(poster: $poster, title: $title)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _MovieSeries &&
            const DeepCollectionEquality().equals(other.poster, poster) &&
            const DeepCollectionEquality().equals(other.title, title));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(poster),
      const DeepCollectionEquality().hash(title));

  @JsonKey(ignore: true)
  @override
  _$MovieSeriesCopyWith<_MovieSeries> get copyWith =>
      __$MovieSeriesCopyWithImpl<_MovieSeries>(this, _$identity);
}

abstract class _MovieSeries implements MovieSeries {
  const factory _MovieSeries(
      {required MoviePoster poster,
      required MovieTitle title}) = _$_MovieSeries;

  @override
  MoviePoster get poster;
  @override
  MovieTitle get title;
  @override
  @JsonKey(ignore: true)
  _$MovieSeriesCopyWith<_MovieSeries> get copyWith =>
      throw _privateConstructorUsedError;
}
