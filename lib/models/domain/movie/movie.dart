import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:majootestcase/models/domain/movie/value_objects.dart';

part 'movie.freezed.dart';

@freezed
class Movie with _$Movie {
  const factory Movie(
      {required MovieTitle title,
      required MoviePoster poster,
      required MovieCategory category,
      required MovieYear year,
      required KtList<MovieSeries> series}) = _Movie;

  factory Movie.empty() => Movie(
        title: MovieTitle(''),
        poster: MoviePoster(''),
        category: MovieCategory(''),
        year: MovieYear(''),
        series: const KtList.empty(),
      );
}

@freezed
class MovieSeries with _$MovieSeries {
  const factory MovieSeries({
    required MoviePoster poster,
    required MovieTitle title,
  }) = _MovieSeries;

  factory MovieSeries.empty() => MovieSeries(
        poster: MoviePoster(''),
        title: MovieTitle(''),
      );
}
