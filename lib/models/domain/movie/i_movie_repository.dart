import 'package:dartz/dartz.dart';
import 'package:kt_dart/collection.dart';
import 'package:majootestcase/models/domain/exception/exceptions.dart';
import 'package:majootestcase/models/domain/movie/movie.dart';

abstract class IMovieRepository {
  Future<Either<AppException, KtList<Movie>>> loadMovies();
}
