import 'package:dartz/dartz.dart';
import 'package:majootestcase/models/domain/core/failures.dart';
import 'package:majootestcase/models/domain/core/value_objects.dart';
import 'package:majootestcase/models/domain/core/value_validators.dart';

class MovieTitle extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory MovieTitle(String input) {
    return MovieTitle._(
      validateStringSingleLine(input).flatMap(validateStringNotEmpty),
    );
  }

  const MovieTitle._(this.value);
}

class MovieCategory extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory MovieCategory(String input) {
    return MovieCategory._(
      validateStringSingleLine(input).flatMap(validateStringNotEmpty),
    );
  }

  const MovieCategory._(this.value);
}

class MovieYear extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory MovieYear(String input) {
    return MovieYear._(
      validateStringSingleLine(input).flatMap(validateStringNotEmpty),
    );
  }

  const MovieYear._(this.value);
}

class MoviePoster extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory MoviePoster(String input) {
    return MoviePoster._(
      validateStringSingleLine(input)
          .flatMap(validateStringNotEmpty)
          .flatMap(validateStringUrl),
    );
  }

  const MoviePoster._(this.value);
}
