import 'package:dartz/dartz.dart';
import 'package:majootestcase/models/domain/core/failures.dart';
import 'package:string_validator/string_validator.dart';

Either<ValueFailure<String>, String> validateEmailAddress(String input) {
  if (isEmail(input)) {
    return right(input);
  }

  return left(ValueFailure.invalidEmail(failedValue: input));
}

Either<ValueFailure<String>, String> validatePassword(String input) {
  if (input.length >= 6) {
    return right(input);
  }

  return left(ValueFailure.shortPassword(failedValue: input));
}

Either<ValueFailure<String>, String> validateStringNotEmpty(String input) {
  if (input.isNotEmpty) {
    return right(input);
  }

  return left(ValueFailure.empty(failedValue: input));
}

Either<ValueFailure<String>, String> validateStringSingleLine(String input) {
  if (input.contains('\n')) {
    return left(ValueFailure.multiLine(failedValue: input));
  }

  return right(input);
}

Either<ValueFailure<String>, String> validateMaxStringLength(
  String input,
  int maxLength,
) {
  if (input.length <= maxLength) {
    return right(input);
  } else {
    return left(ValueFailure.exceedingLength(
      failedValue: input,
      max: maxLength,
    ));
  }
}

Either<ValueFailure<num>, num> validateNominalValue(num input) {
  if (input > 0) {
    return right(input);
  } else {
    return left(ValueFailure.invalidNominal(failedValue: input));
  }
}

Either<ValueFailure<String>, String> validateCredentials(String input) {
  if (input.contains(' ')) {
    return left(ValueFailure.containSpaces(failedValue: input));
  } else {
    return right(input);
  }
}

Either<ValueFailure<String>, String> validateStringUrl(String input) {
  if (isURL(input)) {
    return right(input);
  }
  return left(ValueFailure.multiLine(failedValue: input));
}
