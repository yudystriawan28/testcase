import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:majootestcase/models/domain/core/value_objects.dart';
import 'package:majootestcase/models/domain/user/value_objects.dart';

part 'user.freezed.dart';

@freezed
class User with _$User {
  const factory User({
    required UniqueId id,
    required Username username,
    required Email email,
    required Password password,
  }) = _User;

  factory User.empty() => User(
        id: UniqueId(),
        username: Username(''),
        email: Email(''),
        password: Password(''),
      );
}
