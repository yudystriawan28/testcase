import 'package:dartz/dartz.dart';
import 'package:majootestcase/models/domain/core/failures.dart';
import 'package:majootestcase/models/domain/core/value_objects.dart';
import 'package:majootestcase/models/domain/core/value_validators.dart';

class Email extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory Email(String input) {
    return Email._(
      validateStringSingleLine(input)
          .flatMap(validateStringNotEmpty)
          .flatMap(validateEmailAddress)
          .flatMap(validateCredentials),
    );
  }

  const Email._(this.value);
}

class Username extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory Username(String input) {
    return Username._(
      validateStringSingleLine(input)
          .flatMap(validateStringNotEmpty)
          .flatMap(validateCredentials),
    );
  }

  const Username._(this.value);
}

class Password extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory Password(String input) {
    return Password._(
      validateStringNotEmpty(input)
          .flatMap(validateStringSingleLine)
          .flatMap(validatePassword),
    );
  }

  const Password._(this.value);
}
