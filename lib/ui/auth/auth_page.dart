import 'package:auto_route/auto_route.dart';
import 'package:drift_db_viewer/drift_db_viewer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/auth/auth_cubit.dart';
import 'package:majootestcase/bloc/auth/form/auth_form_cubit.dart';
import 'package:majootestcase/injection.dart';
import 'package:majootestcase/services/database/database.dart';

import 'package:majootestcase/ui/auth/widgets/email_field.dart';
import 'package:majootestcase/ui/auth/widgets/password_field.dart';
import 'package:majootestcase/ui/auth/widgets/username_field.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/extra/toast.dart';
import 'package:majootestcase/ui/routes/app_router.gr.dart';

class AuthPage extends StatelessWidget implements AutoRouteWrapper {
  const AuthPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<AuthFormCubit, AuthFormState>(
          listenWhen: (p, c) =>
              p.failureOrSuccessOption != c.failureOrSuccessOption,
          listener: (context, state) {
            state.failureOrSuccessOption.fold(
              () {},
              (either) => either.fold(
                (f) => showErrorToast(f),
                (_) {
                  if (state.isLogin) {
                    context.read<AuthCubit>().fetchHistoryLogin();
                  } else {
                    Fluttertoast.showToast(
                      msg: 'Register success',
                      backgroundColor: Colors.green,
                      textColor: Colors.white,
                    );
                    context.read<AuthFormCubit>().isLoginChanged(true);
                  }
                },
              ),
            );
          },
        ),
        BlocListener<AuthCubit, AuthState>(
          listenWhen: (p, c) => p.status != c.status,
          listener: (context, state) {
            if (state.status == AuthStatus.authenticated) {
              AutoRouter.of(context).replace(const HomeRoute());
            }
          },
        ),
      ],
      child: BlocBuilder<AuthFormCubit, AuthFormState>(
        buildWhen: (p, c) => p.isSubmitting != c.isSubmitting,
        builder: (context, state) {
          return Stack(
            children: [
              const _AuthFormScaffold(),
              if (state.isSubmitting) const LoadingIndicator(),
            ],
          );
        },
      ),
    );
  }

  @override
  Widget wrappedRoute(BuildContext context) => BlocProvider(
        create: (context) => getIt<AuthFormCubit>(),
        child: this,
      );
}

class _AuthFormScaffold extends StatelessWidget {
  const _AuthFormScaffold({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(
            vertical: 16,
            horizontal: 20,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text(
                'Selamat Datang',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  // color: colorBlue,
                ),
              ),
              BlocBuilder<AuthFormCubit, AuthFormState>(
                buildWhen: (p, c) => p.isLogin != c.isLogin,
                builder: (context, state) {
                  return Text(
                    'Silahkan ${state.isLogin ? 'login' : 'Register'} terlebih dahulu',
                    style: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                  );
                },
              ),
              const SizedBox(
                height: 9,
              ),
              BlocBuilder<AuthFormCubit, AuthFormState>(
                buildWhen: (p, c) => p.showErrorMessages != c.showErrorMessages,
                builder: (context, state) {
                  return Form(
                    autovalidateMode: state.showErrorMessages
                        ? AutovalidateMode.always
                        : AutovalidateMode.disabled,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const EmailField(),
                        const SizedBox(
                          height: 12,
                        ),
                        BlocBuilder<AuthFormCubit, AuthFormState>(
                          buildWhen: (p, c) => p.isLogin != c.isLogin,
                          builder: (context, state) {
                            if (!state.isLogin) {
                              return Column(
                                children: const [
                                  UsernameField(),
                                  SizedBox(
                                    height: 12,
                                  ),
                                ],
                              );
                            }
                            return const SizedBox();
                          },
                        ),
                        const PasswordField(),
                      ],
                    ),
                  );
                },
              ),
              const SizedBox(
                height: 50,
              ),
              SizedBox(
                width: double.infinity,
                child: BlocBuilder<AuthFormCubit, AuthFormState>(
                  buildWhen: (p, c) => p.isLogin != c.isLogin,
                  builder: (context, state) {
                    return ElevatedButton(
                      onPressed: () =>
                          context.read<AuthFormCubit>().submitted(),
                      child: Text(state.isLogin ? 'Login' : 'Register'),
                    );
                  },
                ),
              ),
              BlocBuilder<AuthFormCubit, AuthFormState>(
                buildWhen: (p, c) => p.isLogin != c.isLogin,
                builder: (context, state) {
                  return TextButton(
                    onPressed: () => context
                        .read<AuthFormCubit>()
                        .isLoginChanged(!state.isLogin),
                    child: RichText(
                      text: TextSpan(
                        text: state.isLogin
                            ? 'Belum punya akun? '
                            : 'Sudah punya akun? ',
                        style: const TextStyle(color: Colors.black45),
                        children: [
                          TextSpan(
                            text: state.isLogin ? 'Daftar' : 'Masuk',
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: kDebugMode
          ? FloatingActionButton(
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => DriftDbViewer(getIt<MyDatabase>()),
                ),
              ),
              child: const Icon(Icons.storage),
            )
          : null,
    );
  }
}
