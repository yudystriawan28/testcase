import 'package:flutter/material.dart';
import 'package:majootestcase/bloc/auth/form/auth_form_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EmailField extends StatelessWidget {
  const EmailField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomeTextFormField(
      label: 'Email',
      isMandatory: true,
      onChanged: (value) => context.read<AuthFormCubit>().emailChanged(value),
      validator: (value) =>
          context.read<AuthFormCubit>().state.email.value.fold(
                (f) => f.maybeMap(
                  orElse: () {},
                  invalidEmail: (_) => 'email invalid',
                  empty: (_) => 'tidak boleh kosong',
                ),
                (_) {},
              ),
    );
  }
}
