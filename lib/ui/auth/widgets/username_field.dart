import 'package:flutter/material.dart';
import 'package:majootestcase/bloc/auth/form/auth_form_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UsernameField extends StatelessWidget {
  const UsernameField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomeTextFormField(
      label: 'Username',
      isMandatory: true,
      onChanged: (value) =>
          context.read<AuthFormCubit>().usernameChanged(value),
      validator: (value) =>
          context.read<AuthFormCubit>().state.username.value.fold(
                (f) => f.maybeMap(
                  orElse: () {},
                  empty: (_) => 'tidak boleh kosong',
                ),
                (_) {},
              ),
    );
  }
}
