import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:majootestcase/bloc/auth/form/auth_form_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PasswordField extends HookWidget {
  const PasswordField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final obscureText = useState(true);
    return CustomeTextFormField(
      label: 'Password',
      isMandatory: true,
      onChanged: (value) =>
          context.read<AuthFormCubit>().passwordChanged(value),
      validator: (_) => context.read<AuthFormCubit>().state.password.value.fold(
            (f) => f.maybeMap(
              orElse: () {},
              shortPassword: (_) => 'Minimal 6 karakter',
              empty: (_) => 'Tidak boleh kosong',
            ),
            (_) {},
          ),
      obscureText: obscureText.value,
      suffixIcon: IconButton(
          onPressed: () => obscureText.value = !obscureText.value,
          icon: Icon(
              obscureText.value ? Icons.visibility : Icons.visibility_off)),
    );
  }
}
