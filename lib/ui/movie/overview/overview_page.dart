import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/movie/cubit/movie_loader_cubit.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'widgets/card_movie.dart';

class MovieOverview extends StatefulWidget {
  const MovieOverview({Key? key}) : super(key: key);

  @override
  State<MovieOverview> createState() => _MovieOverviewState();
}

class _MovieOverviewState extends State<MovieOverview> {
  final _refreshController = RefreshController();

  void _onRefresh() async {
    context.read<MovieLoaderCubit>().fetchMovies();
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieLoaderCubit, MovieLoaderState>(
      builder: (context, state) {
        return state.map(
          initial: (_) => const SizedBox(),
          loadInProgress: (_) => const LoadingIndicator(),
          loadFailure: (state) => Center(
            child: ErrorScreen(
              failures: state.failure,
              retryCallback: () =>
                  context.read<MovieLoaderCubit>().fetchMovies(),
            ),
          ),
          loadSuccess: (state) {
            if (state.movies.isEmpty()) {
              return const Text('Data kosong');
            }

            return SmartRefresher(
              controller: _refreshController,
              onRefresh: _onRefresh,
              child: ListView.separated(
                padding: const EdgeInsets.symmetric(
                  vertical: 16,
                  horizontal: 20,
                ),
                itemCount: state.movies.size,
                separatorBuilder: (BuildContext context, int index) {
                  return const SizedBox(
                    height: 12,
                  );
                },
                itemBuilder: (BuildContext context, int index) {
                  final movie = state.movies[index];
                  return CardMovie(movie: movie);
                },
              ),
            );
          },
        );
      },
    );
  }
}
