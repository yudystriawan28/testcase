import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/models/domain/movie/movie.dart';
import 'package:majootestcase/ui/routes/app_router.gr.dart';

class CardMovie extends StatelessWidget {
  const CardMovie({
    Key? key,
    required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => AutoRouter.of(context).push(MovieDetailRoute(movie: movie)),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListTile(
            leading: Image.network(movie.poster.getOrElse('')),
            title: Text(movie.title.getOrCrash()),
            subtitle: Text(
                '${movie.category.getOrCrash()}, ${movie.year.getOrCrash()}'),
          ),
        ),
      ),
    );
  }
}
