import 'package:flutter/material.dart';

import 'package:majootestcase/models/domain/movie/movie.dart';

class MovieDetailPage extends StatelessWidget {
  const MovieDetailPage({
    Key? key,
    required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail Movie'),
      ),
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) => [
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.network(
                    movie.poster.getOrElse(''),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Text(
                    movie.title.getOrCrash(),
                    style: Theme.of(context).textTheme.headline3,
                  ),
                  Text(
                      '${movie.category.getOrCrash()}, ${movie.year.getOrCrash()}'),
                ],
              ),
            ),
          ),
        ],
        body: movie.series.isEmpty()
            ? const Center(child: Text('Series tidak ditemukan'))
            : SingleChildScrollView(
                padding: const EdgeInsets.symmetric(
                  vertical: 16,
                  horizontal: 20,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Series',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: movie.series.size,
                      separatorBuilder: (BuildContext context, int index) {
                        return const SizedBox(
                          height: 12,
                        );
                      },
                      itemBuilder: (BuildContext context, int index) {
                        final series = movie.series[index];
                        return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListTile(
                              leading:
                                  Image.network(series.poster.getOrElse('')),
                              title: Text(
                                series.title.getOrCrash(),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
