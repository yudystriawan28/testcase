import 'package:auto_route/annotations.dart';
import 'package:majootestcase/ui/auth/auth_page.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/movie/detail/detail_page.dart';
import 'package:majootestcase/ui/splash/splash_page.dart';

@AdaptiveAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: [
    AutoRoute(page: SplashPage, initial: true),
    AutoRoute(page: AuthPage),
    AutoRoute(page: HomePage),
    AutoRoute(page: MovieDetailPage),
  ],
)
class $AppRouter {}
