import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth/auth_cubit.dart';
import 'package:majootestcase/bloc/movie/cubit/movie_loader_cubit.dart';
import 'package:majootestcase/injection.dart';
import 'package:majootestcase/ui/extra/dialogs.dart';
import 'package:majootestcase/ui/movie/overview/overview_page.dart';
import 'package:majootestcase/ui/routes/app_router.gr.dart';

class HomePage extends StatelessWidget implements AutoRouteWrapper {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listenWhen: (p, c) => p.status != c.status,
      listener: (context, state) {
        if (state.status == AuthStatus.unauthenticated) {
          AutoRouter.of(context).replace(const AuthRoute());
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Home'),
          actions: [
            IconButton(
              onPressed: () => showAlertDialog(
                context: context,
                title: const Text('Keluar aplikasi?'),
                content: const Text('Yakin ingin keluar aplikasi?'),
              ).then(
                (value) {
                  if (value != null && value) {
                    context.read<AuthCubit>().logout();
                  }
                },
              ),
              icon: const Icon(Icons.exit_to_app),
            ),
          ],
        ),
        body: const MovieOverview(),
      ),
    );
  }

  @override
  Widget wrappedRoute(BuildContext context) => BlocProvider(
        create: (context) => getIt<MovieLoaderCubit>()..fetchMovies(),
        child: this,
      );
}
