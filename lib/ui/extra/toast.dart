import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/models/domain/exception/exceptions.dart';

void showErrorToast(AppException exception) {
  Fluttertoast.showToast(
    msg: exception.map(
      serverError: (ex) => ex.errorMessage ?? 'server error',
      unexpected: (ex) => ex.errorMessage ?? 'Terjadi kesalahan',
      unauthenticated: (_) => 'Login gagal, periksa kembali inputan anda',
      badNetwork: (_) => 'Koneksi bermasalah coba lagi nanti',
    ),
    textColor: Colors.white,
    backgroundColor: Colors.red,
  );
}

void showSuccessToast(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    textColor: Colors.white,
    backgroundColor: Colors.green,
  );
}
