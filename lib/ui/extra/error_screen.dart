import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:majootestcase/bloc/auth/auth_cubit.dart';
import 'package:majootestcase/models/domain/exception/exceptions.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({
    Key? key,
    required this.failures,
    this.retryCallback,
  }) : super(key: key);

  final AppException failures;
  final VoidCallback? retryCallback;

  @override
  Widget build(BuildContext context) {
    return failures.map(
      serverError: (state) => Text(state.errorMessage ?? 'Server error'),
      unexpected: (state) => Text(state.errorMessage ?? 'Terjadi kesalahan'),
      unauthenticated: (_) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text('Session habis'),
          TextButton(
            onPressed: () => context.read<AuthCubit>().logout(),
            child: const Text('Logout'),
          )
        ],
      ),
      badNetwork: (_) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            Icons.refresh,
            size: 64,
          ),
          const SizedBox(
            height: 8,
          ),
          const Text('Koneksi bermasalah coba lagi nanti'),
          const SizedBox(
            height: 12,
          ),
          TextButton(
            onPressed: retryCallback,
            child: const Text('Retry'),
          ),
        ],
      ),
    );
  }
}
