import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

Future<bool?> showAlertDialog({
  required BuildContext context,
  required Widget title,
  required Widget content,
}) {
  return showDialog<bool?>(
    context: context,
    builder: (context) => AlertDialog(
      title: title,
      content: content,
      actions: [
        TextButton(
          onPressed: () => AutoRouter.of(context).pop(true),
          child: const Text('Iya'),
        ),
        TextButton(
          onPressed: () => AutoRouter.of(context).pop(false),
          child: const Text(
            'Tidak jadi',
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
    ),
  );
}
