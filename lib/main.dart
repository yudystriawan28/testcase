import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/injection.dart';
import 'package:majootestcase/ui/routes/app_router.gr.dart';

import 'bloc/auth/auth_cubit.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await configureDependencies(Environment.dev);

  runApp(BlocProvider(
    create: (context) => getIt<AuthCubit>()..fetchHistoryLogin(),
    child: const MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.

  late AppRouter _appRouter;

  @override
  void initState() {
    super.initState();
    _appRouter = AppRouter();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: _appRouter.defaultRouteParser(),
      routerDelegate: AutoRouterDelegate(_appRouter),
      title: 'Flutter Demo',
      builder: (context, child) => child!,
    );
  }
}

// class MyHomePageScreen extends StatelessWidget {
//   const MyHomePageScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
//       if (state is AuthBlocLoginState) {
//         return LoginPage();
//       } else if (state is AuthBlocLoggedInState) {
//         return BlocProvider(
//           create: (context) => HomeBlocCubit()..fetchingData(),
//           child: HomeBlocScreen(),
//         );
//       }

//       return Center(
//           child: Text(kDebugMode ? "state not implemented $state" : ""));
//     });
//   }
// }
