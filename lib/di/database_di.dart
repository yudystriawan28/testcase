import 'package:injectable/injectable.dart';
import 'package:majootestcase/services/database/config/connection.dart';
import 'package:majootestcase/services/database/database.dart';

@module
abstract class DatabaseDi {
  @singleton
  MyDatabase get db => MyDatabase.connect(createDriftIsolateAndConnect());
}
