part of 'movie_loader_cubit.dart';

@freezed
class MovieLoaderState with _$MovieLoaderState {
  const factory MovieLoaderState.initial() = _Initial;
  const factory MovieLoaderState.loadInProgress() = _LoadInProgress;
  const factory MovieLoaderState.loadFailure(AppException failure) =
      _LoadFailure;
  const factory MovieLoaderState.loadSuccess(KtList<Movie> movies) =
      _LoadSuccess;
}
