import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/collection.dart';

import 'package:majootestcase/models/domain/exception/exceptions.dart';
import 'package:majootestcase/models/domain/movie/i_movie_repository.dart';
import 'package:majootestcase/models/domain/movie/movie.dart';

part 'movie_loader_cubit.freezed.dart';
part 'movie_loader_state.dart';

@injectable
class MovieLoaderCubit extends Cubit<MovieLoaderState> {
  MovieLoaderCubit(
    this._movieRepository,
  ) : super(const MovieLoaderState.initial());

  final IMovieRepository _movieRepository;

  void fetchMovies() async {
    emit(const MovieLoaderState.loadInProgress());

    final failureOrSuccess = await _movieRepository.loadMovies();

    emit(failureOrSuccess.fold(
      (f) => MovieLoaderState.loadFailure(f),
      (movies) => MovieLoaderState.loadSuccess(movies),
    ));
  }
}
