part of 'auth_cubit.dart';

enum AuthStatus {
  authenticated,
  unauthenticated,
  unknown,
  loading,
  loggedOut,
}

@freezed
class AuthState with _$AuthState {
  const factory AuthState({
    required User user,
    required AuthStatus status,
  }) = _AuthState;

  factory AuthState.initial() => AuthState(
        user: User.empty(),
        status: AuthStatus.unknown,
      );
}
