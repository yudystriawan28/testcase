import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/domain/auth/i_auth_repository.dart';
import 'package:majootestcase/models/domain/core/value_objects.dart';
import 'package:majootestcase/models/domain/exception/exceptions.dart';
import 'package:majootestcase/models/domain/user/user.dart';
import 'package:majootestcase/models/domain/user/value_objects.dart';

part 'auth_form_cubit.freezed.dart';
part 'auth_form_state.dart';

@injectable
class AuthFormCubit extends Cubit<AuthFormState> {
  AuthFormCubit(
    this._authRepository,
  ) : super(AuthFormState.initial());

  final IAuthRepository _authRepository;

  void emailChanged(String emailStr) {
    emit(state.copyWith(
      email: Email(emailStr),
      failureOrSuccessOption: none(),
    ));
  }

  void passwordChanged(String passwordStr) {
    emit(state.copyWith(
      password: Password(passwordStr),
      failureOrSuccessOption: none(),
    ));
  }

  void usernameChanged(String usernameStr) {
    emit(state.copyWith(
      username: Username(usernameStr),
      failureOrSuccessOption: none(),
    ));
  }

  void isLoginChanged(bool isLogin) {
    emit(state.copyWith(
      isLogin: isLogin,
      showErrorMessages: false,
      failureOrSuccessOption: none(),
    ));
  }

  void submitted() async {
    Either<AppException, Unit>? failureOrSuccess;

    emit(state.copyWith(
      isSubmitting: true,
      failureOrSuccessOption: none(),
    ));

    if (state.isLogin) {
      if (state.email.isValid() && state.password.isValid()) {
        failureOrSuccess = await _authRepository.login(
          email: state.email,
          password: state.password,
        );
      }
    } else {
      if (state.email.isValid() &&
          state.password.isValid() &&
          state.username.isValid()) {
        final user = User(
          id: UniqueId(),
          username: state.username,
          email: state.email,
          password: state.password,
        );
        failureOrSuccess = await _authRepository.register(user);
      }
    }

    emit(state.copyWith(
      isSubmitting: false,
      showErrorMessages: true,
      failureOrSuccessOption: optionOf(failureOrSuccess),
    ));
  }
}
