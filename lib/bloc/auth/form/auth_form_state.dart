part of 'auth_form_cubit.dart';

@freezed
class AuthFormState with _$AuthFormState {
  const factory AuthFormState({
    required Email email,
    required Password password,
    required Username username,
    required Option<Either<AppException, Unit>> failureOrSuccessOption,
    @Default(false) bool isSubmitting,
    @Default(false) bool showErrorMessages,
    @Default(true) bool isLogin,
  }) = _AuthFormState;

  factory AuthFormState.initial() => AuthFormState(
        email: Email(''),
        password: Password(''),
        username: Username(''),
        failureOrSuccessOption: none(),
      );
}
