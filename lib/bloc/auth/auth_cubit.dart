import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/domain/auth/i_auth_repository.dart';
import 'package:majootestcase/models/domain/core/value_objects.dart';
import 'package:majootestcase/models/domain/user/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_cubit.freezed.dart';
part 'auth_state.dart';

@injectable
class AuthCubit extends Cubit<AuthState> {
  AuthCubit(
    this._prefs,
    this._authRepository,
  ) : super(AuthState.initial());

  final SharedPreferences _prefs;
  final IAuthRepository _authRepository;

  void fetchHistoryLogin() async {
    emit(state.copyWith(
      status: AuthStatus.loading,
    ));

    final userToken = _prefs.getString(kToken) ?? '';

    final failureOrSuccess =
        await _authRepository.loadUser(UniqueId.fromUniqueString(userToken));

    emit(failureOrSuccess.fold(
      (l) => state.copyWith(
        status: AuthStatus.unauthenticated,
      ),
      (user) => state.copyWith(
        status: AuthStatus.authenticated,
        user: user,
      ),
    ));
  }

  Future<void> logout() async {
    emit(state.copyWith(status: AuthStatus.loading));

    await _authRepository.logout();

    emit(state.copyWith(
      status: AuthStatus.unauthenticated,
      user: User.empty(),
    ));
  }
}
